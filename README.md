# MqSensor

Simple library for MQ-2 sensor for Arduino.

## Links

* Project homepage: [hosted on GitLab Pages](
    https://open-kappa.gitlab.io/arduino/mqsensor)
* Project sources: [hosted on gitlab.com](
    https://gitlab.com/open-kappa/arduino/mqsensor)
* List of open-kappa projects, [hosted on GitLab Pages](
    https://open-kappa.gitlab.io)

## License

MqSensor is released under the liberal MIT License.
Please refer to the LICENSE.txt project file for further details.

## Patrons

This Arduino library has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
