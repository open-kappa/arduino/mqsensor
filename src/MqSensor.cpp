#include "MqSensor.hpp"
#include <Arduino.h>

using mqsensor::MqSensor;

MqSensor::MqSensor():
    _pin(0),
    _r0(0.0),
    _rl(0.0),
    _maxAnalogRead(0.0),
    _sensorInputVoltage(0.0)
{}

// NOLINTNEXTLINE(hicpp-use-equals-default,modernize-use-equals-default)
MqSensor::~MqSensor()
{}

auto MqSensor::setup(const MqSensorConfig & config) -> bool
{
    _pin = config.pin;
    _r0 = config.r0;
    _rl = config.rl;
    _maxAnalogRead = config.maxAnalogRead;
    _sensorInputVoltage = config.sensorInputVoltage;
    adcAttachPin(_pin);
    if (!_calibrate(config)) return false;
    _initializeMq(config);
    return true;
}

auto MqSensor::read(MqData & data) const -> bool
{
    const auto readData = analogRead(_pin);
    // Proportion: read value to voltage
    const auto sensorVoltage = readData * _sensorInputVoltage / _maxAnalogRead;
    if (sensorVoltage == 0.0)
    {
        data.propaneC3h8Ppm = 0;
        data.methaneCh3Ppm = 0;
        data.alchoolPpm = 0;
        data.smokePpm = 0;
        return true;
    }
    // From datasheet schematic:
    // V_out = Rl / (Rs + Rl) * V_in
    const auto rs = _sensorInputVoltage *_rl / sensorVoltage - _rl;
    const auto rsR0Ratio = rs / _r0;
    if (rsR0Ratio <= 0) return false;
    // From datasheet, this is log-linear:
    // log(y) = m * log(x) + q, so:
    // x = 10^((log(y) - q) / m)
    const auto logRatio = log10(rsR0Ratio);
    data.propaneC3h8Ppm = exp10((logRatio - _mqPropaneC3h8.q) / _mqPropaneC3h8.m);
    data.methaneCh3Ppm = exp10((logRatio - _mqMethaneCh4.q) / _mqMethaneCh4.m);
    data.alchoolPpm = exp10((logRatio - _mqAlchool.q) / _mqAlchool.m);
    data.smokePpm = exp10((logRatio - _mqSmoke.q) / _mqSmoke.m);
    return true;
}

auto MqSensor::getR0() const -> double
{
    return _r0;
}

auto MqSensor::_calibrate(const MqSensorConfig & config) -> bool
{
    if (config.calibrationReads == 0) return true;

    // Estimate at fresh air the base read value:
    uint64_t readDataSum = 0;
    for (uint32_t i = 0; i < config.calibrationReads; ++i)
    {
        const auto value = analogRead(_pin);
        readDataSum += value;
    }
    const auto readDataAvg = double(readDataSum) / config.calibrationReads;
    // Proportion: read value to voltage
    const auto sensorVoltage = readDataAvg *  _sensorInputVoltage / _maxAnalogRead;
    if (sensorVoltage == 0)
    {
        _r0 = 0.0;
        return true;
    }
    // From datasheet schematic:
    // V_out = Rl / (Rs + Rl) * V_in
    const auto rs = _sensorInputVoltage *_rl / sensorVoltage - _rl;
    // Rs/R0 = RsR0AirRatio --> R0 = Rs / RsR0AirRatio
    _r0 = rs / config.rsR0AirRatio;
    return true;
}

auto MqSensor::_initializeMq(const MqSensorConfig & config) -> void
{
    _calculateMq(
        _mqPropaneC3h8,
        config.estimatedPoints.propaneC3h8_0,
        config.estimatedPoints.propaneC3h8_1
    );
    _calculateMq(
        _mqMethaneCh4,
        config.estimatedPoints.methaneCh3_0,
        config.estimatedPoints.methaneCh3_1
    );
    _calculateMq(
        _mqAlchool,
        config.estimatedPoints.alchool_0,
        config.estimatedPoints.alchool_1
    );
    _calculateMq(
        _mqSmoke,
        config.estimatedPoints.smoke_0,
        config.estimatedPoints.smoke_1
    );
}

auto MqSensor::_calculateMq(
    Mq & mq,
    const MqPoint & point0,
    const MqPoint & point1
) -> void
{
    constexpr double x_1000 = 1000.0;
    constexpr double x_10000 = 10000.0;
    if (point0.xConcentration == x_1000 && point1.xConcentration == x_10000)
    {
        _calculateMq_10000_1000(
            mq,
            point1.yRsR0Ratio,
            point0.yRsR0Ratio
        );
    }
    else if (point0.xConcentration == x_10000 && point1.xConcentration == x_1000)
    {
        _calculateMq_10000_1000(
            mq,
            point0.yRsR0Ratio,
            point1.yRsR0Ratio
        );
    }
    else
    {
        _calculateMqGeneric(
            mq,
            point0,
            point1
        );
    }
}

auto MqSensor::_calculateMqGeneric(
    Mq & mq,
    const MqPoint & point0,
    const MqPoint & point1
) -> void
{
    // From datasheet, this is log-linear:
    // log(y) = m * log(x) + q

    // So this is: m =
    // = (log(y0) - log(y1)) / (log(x0) - log(x1))
    // = log(y0/y1) / log(x0/x1)
    mq.m = std::log10(point0.yRsR0Ratio / point1.yRsR0Ratio)
        / std::log10(point0.xConcentration / point1.xConcentration);
    // q = log(y) - m * log(x) = log(y / x^m)
    mq.q = std::log10(point1.yRsR0Ratio / std::pow(point1.xConcentration, mq.m));
}

auto MqSensor::_calculateMq_10000_1000(
    Mq & mq,
    const double y0RsR0Ratio_10000,
    const double y1RsR0Ratio_1000
) -> void
{
    // From datasheet, this is log-linear:
    // log(y) = m * log(x) + q

    // So this is: m =
    // = (log(y0) - log(y1)) / (log(x0) - log(x1))
    // = log(y0/y1) / log(x0/x1)
    // = log(y0/y1) / log(10000/1000)
    // = log(y0/y1) / log(10)
    // = log(y0/y1) / 1
    mq.m = std::log10(y0RsR0Ratio_10000 / y1RsR0Ratio_1000);
    // q = log(y) - m * log(x)
    // = log(y1) - m * log(x1)
    // = log(y1) - m * log(1000)
    // = log(y1) - m * 3
    mq.q = std::log10(y1RsR0Ratio_1000) - mq.m * 3.0;
}
