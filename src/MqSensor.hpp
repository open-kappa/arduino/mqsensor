#ifndef MQSENSOR_MQSENSOR_HPP
#define MQSENSOR_MQSENSOR_HPP

#include <cstdint>

namespace mqsensor {

/// @brief Struct to pass sensor data.
/// Data values are in PPM.
struct MqData
{
    double propaneC3h8Ppm = 0.0;
    double methaneCh3Ppm = 0.0;
    double alchoolPpm = 0.0;
    double smokePpm = 0.0;
};

/// @brief A point in the datasheet's chart.
struct MqPoint
{
    /// @brief Constructor.
    MqPoint() = default;

    /// @brief Constructor.
    MqPoint(const double x, const double y):
        xConcentration(x),
        yRsR0Ratio(y)
    {}

    /// The x of the first point.
    double xConcentration = 0.0;
    /// The y of the first point.
    double yRsR0Ratio = 0.0;
};

/// @brief Points in the datasheet's chart.
/// The points have some default values, but it is suggested to check the
/// datasheet.
struct MqEstimatedPoints
{
    MqPoint propaneC3h8_0 = {1000, 0.095};
    MqPoint propaneC3h8_1 = {10000, 0.019};
    MqPoint methaneCh3_0 = {1000, 0.16};
    MqPoint methaneCh3_1 = {10000, 0.043};
    MqPoint alchool_0 = {1000, 0.22};
    MqPoint alchool_1 = {10000, 0.061};
    MqPoint smoke_0 = {1000, 0.11};
    MqPoint smoke_1 = {10000, 0.023};
};


/// @brief The configuration for the MQSensor class.
/// The only mandatory value is the pin number, all the others have been
/// defaulted (but they could change for different suppliers).
struct MqSensorConfig final
{
    /// @brief Constructor.
    MqSensorConfig() = default;
    /// @brief Constructor.
    explicit MqSensorConfig(const uint8_t pin_) :
        pin(pin_)
    {}

    /// The analog pin to read.
    uint8_t pin = 2;
    /// The ratio of RS/R0 in a clear air (read it in your datasheet).
    /// Some examples:
    /// * MQ2: 10
    /// * MQ3: 70
    /// * MQ4: 3
    /// * MQ5: 6.5
    /// * MQ6: 10
    /// * MQ7: 28
    double rsR0AirRatio = 10.0;
    /// The number of read to perform during the calibration phase.
    /// Zero means no calibration.
    uint32_t calibrationReads = 500;
    /// The voltage supply for the sensor.
    double sensorInputVoltage = 5.0;
    /// The RL value from the datasheet.
    double rl = 4.7;
    /// The maximum value for the analog read.
    /// Usually it is 4096 for ESP32 (12 bits).
    double maxAnalogRead = 4096.0;
    /// A default value for R0.
    /// This is useful only if no calibration is performed.
    double r0 = 0.0;
    /// The values of the estimated points from the datasheet chart.
    MqEstimatedPoints estimatedPoints;
};

/// @brief The MqSensor class.
class MqSensor final
{
public:
    /// @brief Support struct to store coefficients.
    struct Mq
    {
        double m = 0.0;
        double q = 0.0;
    };

    /// @brief Constructor.
    /// @param config The configuration.
    MqSensor();

    /// @brief Destructor.
    ~MqSensor();

    /// @brief Perform setup.
    auto setup(const MqSensorConfig & config) -> bool;

    /// @brief Read the values.
    /// @param data The output read data.
    auto read(MqData & data) const -> bool;

    /// @brief Get the estimated R0.
    auto getR0() const -> double;

private:
    /// @brief Perform the calibration.
    auto _calibrate(const MqSensorConfig & config) -> bool;

    /// @brief Initiazlie m and q coefficients.
    auto _initializeMq(const MqSensorConfig & config) -> void;

    /// @brief Calculates the m and q coefficients.
    /// These are the coefficients in the log-linear equation (from the
    /// datasheet): log(y) = m * log(x) + q
    /// The two points must be taken from the datasheet, from the chart.
    /// Unfortunatly, a tabular input would have been more precise.
    /// Support an optimization if the two points are at 1000 and 10000 ppm
    /// of concentration.
    /// @param mq The output m and q coefficients.
    /// @param point0 The first point.
    /// @param point1 The second point.
    static auto _calculateMq(
        Mq & mq,
        const MqPoint & point0,
        const MqPoint & point1
    ) -> void;

    /// @brief Calculates the m and q coefficients.
    /// Generic formula.
    /// @param mq The output m and q coefficients.
    /// @param point0 The first point.
    /// @param point1 The second point.
    static auto _calculateMqGeneric(
        Mq & mq,
        const MqPoint & point0,
        const MqPoint & point1
    ) -> void;

    /// @brief Calculates the m and q coefficients.
    /// Optimized formula for ratio inputs at 1000 and 10000 ppm
    /// of concentration.
    /// @param mq The output m and q coefficients.
    /// @param y0RsR0Ratio_10000 The y of the first point at 10000 ppm.
    /// @param y1RsR0Ratio_1000 The y of the second point at 1000 ppm.
    static auto _calculateMq_10000_1000(
        Mq & mq,
        const double y0RsR0Ratio_10000,
        const double y1RsR0Ratio_1000
    ) -> void;

    uint8_t _pin;
    double _r0;
    double _rl;
    double _maxAnalogRead;
    double _sensorInputVoltage;
    Mq _mqPropaneC3h8;
    Mq _mqMethaneCh4;
    Mq _mqAlchool;
    Mq _mqSmoke;

    MqSensor(const MqSensor & other) = delete;
    MqSensor(MqSensor && other) = delete;
    MqSensor & operator =(const MqSensor & other) = delete;
    MqSensor & operator =(MqSensor && other) = delete;
};

} // namespace mqsensor

#endif
