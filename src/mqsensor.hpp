#ifndef MQSENSOR_HPP
#define MQSENSOR_HPP

// @brief The namespace for the mqsensor library.
namespace mqsensor {}

#include "MqSensor.hpp"

#endif
