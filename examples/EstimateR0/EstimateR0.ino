#include <mqsensor.hpp>

mqsensor::MqSensor mqSensor;

void setup()
{
    Serial.begin(115200);
    const auto ok = mqSensor.setup(mqsensor::MqSensorConfig(2));
    if (!ok)
    {
        while(true)
        {
            Serial.println("Unable to initialize MQ sensor");
            delay(2000);
        }
    }
}

void loop()
{
    const auto r0 = mqSensor.getR0();
    Serial.print("R0: "); Serial.println(r0);
    delay(2000);
}
