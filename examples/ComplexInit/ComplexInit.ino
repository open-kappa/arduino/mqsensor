#include <mqsensor.hpp>

mqsensor::MqSensor mqSensor;

constexpr unsigned long TIMEOUT_MS = 3000;
unsigned long _previousTimeout = 0;

void setup()
{
    Serial.begin(115200);
    mqsensor::MqSensorConfig config;
    config.pin = 2;
    config.rsR0AirRatio = 9.8;
    config.calibrationReads = 1000;
    config.estimatedPoints.smoke_0 = {1000, 0.1098};
    config.estimatedPoints.smoke_1 = {10000, 0.02287};
    const auto ok = mqSensor.setup(config);
    if (!ok)
    {
        while(true)
        {
            Serial.println("Unable to initialize MQ sensor");
            delay(2000);
        }
    }
}

void loop()
{
    const auto current = millis();
    if (current - _previousTimeout < TIMEOUT_MS) return;
    mqsensor::MqData data;
    mqSensor.read(data);

    Serial.print("Propane (ppm): "); Serial.println(data.propaneC3h8Ppm);
    Serial.print("Methane (ppm): "); Serial.println(data.methaneCh3Ppm);
    Serial.print("Alchool (ppm): "); Serial.println(data.alchoolPpm);
    Serial.print("Smoke (ppm): "); Serial.println(data.smokePpm);
}
